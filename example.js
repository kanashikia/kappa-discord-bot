'use strict';

/**
 * A ping pong bot, whenever you send "ping", it replies "pong".
 */

// Import the discord.js module
const Discord = require('discord.js');
// Import the config file
const { token, prefix } = require('./config.json');

// Create an instance of a Discord client
const client = new Discord.Client();

/**
 * The ready event is vital, it means that only _after_ this will your bot start reacting to information
 * received from Discord
 */
client.on('ready', () => {
	console.log('I am ready!');
});

// Create an event listener for messages
client.on('message', message => {
	if (!message.content.startsWith(prefix) || message.author.bot) return;

	const args = message.content.slice(prefix.length).trim().split(' ');
	const command = args.shift().toLowerCase();

	// If the message is "ping"
	if (command === 'ping') {
		// Send "pong" to the same channel
		message.channel.send('check');
	}

	else if (command === 'infos') {
		message.guild.members.fetch()
			.then(data => {
				for(const [key, value] of data) {
					console.log(key, value);
				}
			});
	}

	else if (command === 'get') {
		message.channel.send(`${args}`);
	}
});

client.login(token);