/* eslint-disable no-inner-declarations */
"use strict";

// Import the discord.js module
const Discord = require("discord.js");
// Import the config file
const { token, prefix } = require("./config.json");
// Import additional modules
const fs = require("fs");
const fetch = require("node-fetch");
const xpathHtml = require("xpath-html");
const ytdl = require("ytdl-core");
const cron = require("cron");

const myIntents = new Discord.Intents(Discord.Intents.ALL); //fuck intents discord

// Create an instance of a Discord client
const client = new Discord.Client({ ws: { intents: myIntents } });

const roleNewUser = "Clients FM";

const unsupported_roles = [
  "MASTER FDP",
  "really old bitches",
  "KappaHD members",
  "Nouveaux",
  "Server Booster",
  "Rocket League Gang",
  "Dév",
  "Rythm",
  "Kappa Bot",
  "Groovy",
  "Clients FM",
];

client.on("guildMemberAdd", (member) => {
  let guild_role = member.guild.roles.cache.find(
    (role) => role.name === roleNewUser
  );

  member.roles.add(guild_role);
  console.log(
    `${member.user.username} a bien été ajouté au role ${roleNewUser}`
  );
  /* Decomment to send a welcome message from bot
  // channel fm
  const defaultChannel = member.guild.channels.cache.get("784018445318225941");
  
  defaultChannel.send(`Bienvenue ${member.user.username} !`);
  */
});

client.on("ready", () => {
  console.log("I am ready!");

  const channelId = "804696971600199680";

  let scheduledMessage = new cron.CronJob("00 00 18 * * *", () => {
    // This runs every day at 18:00:00, you can do anything you want
    grab_url_citation_cron(client, channelId);
  });

  scheduledMessage.start();

  let scheduledMessage2 = new cron.CronJob("00 00 20 * * *", () => {
    grab_url_citation_cron(client, channelId);
  });

  scheduledMessage2.start();
});

// Create an event listener for messages
client.on("message", async (message) => {
  if (!message.content.startsWith(prefix) || message.author.bot) return;

  // Available commands for people who have permissions to manage messages
  if (!message.member.hasPermission("MANAGE_MESSAGES")) return;

  const args = message.content.slice(prefix.length).trim().split(" ");
  const command = args.shift().toLowerCase();
  let all_args_as_one = args.toString().replace(",", " ");
  let first_arg = args[0];

  if (command === "help") {
    message.channel.send(`
    
    Commandes : 
    ping
    cheh nompersonne (gif + fais venir un gif audio dans le channel si tu es dans un channel audio)
    clear (admin)
    admin (admin)
    cdj (Citation du jour + audio)
    lcdj (Donne le lien de la citation du jour)
    pytb lienyoutube (Joue la video youtube)
    `);
  }

  // If the message is "ping"
  if (command === "ping") {
    // Send "pong" to the same channel
    message.channel.send("pong");
  }

  if (command === "uwu") {
    message.channel.send(`⎝⎠ ╲╱╲╱ ⎝⎠
    
    https://media.giphy.com/media/ehwuBgKNA2NACoFa7w/giphy.gif`);
    //play_ytb(message, "https://youtu.be/LDU_Txk06tM?t=74", 6);
  }

  // Invoke groovy to play the "citation du jour"
  if (command === "cdj") {
    grab_url_citation(message);
  }

  if (command === "lcdj") {
    getCitationLink(message);
  }

  if (command === "pytb") {
    play_ytb(message, first_arg);
  }

  if (command === "cheh") {
    message.channel.send(`CHEH ${first_arg} 
    https://media.giphy.com/media/Pk3XN88POOAzTp8hU3/giphy.gif`);
    play_ytb(message, "https://www.youtube.com/watch?v=9M2Ce50Hle8");
  }

  // Available commands for Administrators
  if (!message.member.hasPermission("ADMINISTRATOR")) return;
  if (command === "admin") {
    // Send "pong" to the same channel
    message.channel.send("check admin");
  }

  //Clear 100 messages from a channel (100 being the maximum)
  if (command === "clear") {
    async function clear() {
      message.delete();
      const fetched = await message.channel.messages.fetch({
        limit: 99,
      });
      message.channel.bulkDelete(fetched);
    }
    clear();
  }
});

client.login(token);

function play_ytb(message, first_arg) {
  try {
    message.member.voice.channel
      .join()
      .then((connection) => {
        const dispatcher = connection.play(
          ytdl(first_arg, { filter: "audioonly" }, { bitrate: "auto" })
        );

        dispatcher.on("finish", (end) => {
          message.member.voice.channel.leave();
          dispatcher.destroy();
        });
      })
      .catch((e) => console.log(e));
  } catch (error) {
    console.log("N'est pas dans un channel audio");
  }
}

function play_ytb(message, first_arg, seconds) {
  try {
    message.member.voice.channel
      .join()
      .then((connection) => {
        const dispatcher = connection.play(
          ytdl(first_arg, { filter: "audioonly" }, { bitrate: "auto" })
        );
        dispatcher.on("finish", (end) => {
          message.member.voice.channel.leave();
          dispatcher.destroy();
        });
      })
      .catch((e) => console.log(e));
  } catch (error) {
    console.log("N'est pas dans un channel audio");
  }
}

/**
 * Grab URL from website, get ytb URL, extract audio file and returns it in the message owner channel
 * @param {string} message
 */
function grab_url_citation(message) {
  try {
    const html = fetch("http://george-abitbol.fr/")
      .then((response) => response.text())
      .then((lastResponse) => {
        const nodes = xpathHtml
          .fromPageSource(lastResponse)
          .findElements(`//*[@id="gauche"]/div/iframe`);
        let finalLink = nodes[0].attributes[0].nodeValue;

        const embed = new Discord.MessageEmbed()
          // Set the title of the field
          .setTitle("La citation du jour")
          // Set the color of the embed
          .setColor(0xff0000)
          // Set the main content of the embed
          .setDescription(`${finalLink}`);
        // Send the embed to the same channel as the message

        message.channel.send(embed);

        message.member.voice.channel
          .join()
          .then((connection) => {
            const dispatcher = connection.play(
              ytdl(finalLink, { filter: "audioonly" }, { bitrate: "auto" })
            );

            dispatcher.on("finish", (end) => {
              message.member.voice.channel.leave();
              dispatcher.destroy();
            });
          })
          .catch((e) => console.log(e));
      })
      .catch((e) => console.log(e));
  } catch (error) {
    console.log("N'est pas dans un channel audio");
  }
}

function grab_url_citation_cron(client, channelID) {
  try {
    const html = fetch("http://george-abitbol.fr/")
      .then((response) => response.text())
      .then((lastResponse) => {
        const nodes = xpathHtml
          .fromPageSource(lastResponse)
          .findElements(`//*[@id="gauche"]/div/iframe`);
        let finalLink = nodes[0].attributes[0].nodeValue;

        client.channels.cache
          .get(channelID)
          .join()
          .then((connection) => {
            const dispatcher = connection.play(
              ytdl(finalLink, { filter: "audioonly" }, { bitrate: "auto" })
            );

            dispatcher.on("finish", (end) => {
              client.channels.cache.get(channelID).leave();
              dispatcher.destroy();
            });
          })
          .catch((e) => console.log(e));
      })
      .catch((e) => console.log(e));
  } catch (error) {
    console.log("N'est pas dans un channel audio");
  }
}

/**
 * Grab URL from website, get ytb URL and returns it in the message owner channel
 * @param {string} message
 */
function getCitationLink(message) {
  try {
    const html = fetch("http://george-abitbol.fr/")
      .then((response) => response.text())
      .then((lastResponse) => {
        const nodes = xpathHtml
          .fromPageSource(lastResponse)
          .findElements(`//*[@id="gauche"]/div/iframe`);
        let finalLink = nodes[0].attributes[0].nodeValue;

        const embed = new Discord.MessageEmbed()
          // Set the title of the field
          .setTitle("La citation du jour")
          // Set the color of the embed
          .setColor(0xff0000)
          // Set the main content of the embed
          .setDescription(`${finalLink}`);
        // Send the embed to the same channel as the message
        message.channel.send(embed);
      });
  } catch (error) {
    console.log("Erreur sur le renvoi du lien YTB");
  }
}
/**
 * Delete member role
 * @param {*} message
 * @param {*} input_role
 */
function del_role(message, input_role) {
  try {
    if (!message.guild)
      return message.channel.send("Tu dois faire partie d'une guilde");
    let guild_role = message.member.guild.roles.cache.find(
      (role) => role.name === input_role
    );
    // If role is in the list
    if (guild_role != undefined) {
      if (guild_role) message.member.roles.remove(guild_role);
      message.channel.send("Rôle supprimé avec succès !");
    } else {
      message.channel.send("Le rôle n'existe pas");
    }
  } catch (err) {
    console.error(err);
  }
}

/**
 * Add member role
 * @param {*} message
 * @param {*} input_role
 */
function role(message, input_role) {
  try {
    let guild_role = message.member.guild.roles.cache.find(
      (role) => role.name === input_role
    );
    // If role is in the list
    if (guild_role != undefined && !unsupported_roles.includes(input_role)) {
      message.member.roles.add(guild_role);
      message.channel.send("Rôle ajouté avec succès !");
    } else {
      message.channel.send(
        "Le rôle n'existe pas ou tu ne peux pas t'ajouter ce rôle"
      );
    }
  } catch (err) {
    console.error(err);
  }
}
